import {
  addUserDetails,
  deleteUser,
  editUser,
  getUserById,
  getUsers,
} from "./landing";
import { combineReducers } from "redux";
const rootReducer = combineReducers({
  getUsers: getUsers,
  deleteUser: deleteUser,
  addUser: addUserDetails,
  getUserById: getUserById,
  editUser: editUser,
});
export default rootReducer;
