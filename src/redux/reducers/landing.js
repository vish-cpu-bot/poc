import {
  USERLIST_API_FAILURE,
  USERLIST_API_REQUEST,
  USERLIST_API_SUCCESS,
  USERDELETE_API_REQUEST,
  USERDELETE_API_SUCCESS,
  USERDELETE_API_FAILURE,
  ADDUSER_API_REQUEST,
  ADDUSER_API_SUCCESS,
  ADDUSER_API_FAILURE,
} from "../../constant/constants";

const initialState = {
  users: [],
  isLoading: false,
  error: null,
  refetch: false,
};

function getUsers(state = initialState, action) {
  switch (action.type) {
    case USERLIST_API_REQUEST: {
      return { users: [], isLoading: true, error: null };
    }
    case USERLIST_API_SUCCESS: {
      return { users: [action.payload], isLoading: false, error: null };
    }
    case USERLIST_API_FAILURE: {
      return { users: [], isLoading: false, error: action.payload };
    }
    default:
      return state;
  }
}

function deleteUser(state = initialState, action) {
  switch (action.type) {
    case USERDELETE_API_REQUEST: {
      return { users: [], isLoading: true, error: null, refetch: false };
    }
    case USERDELETE_API_SUCCESS: {
      return {
        users: [action.payload],
        isLoading: false,
        error: null,
        refetch: true,
      };
    }
    case USERDELETE_API_FAILURE: {
      return {
        users: [],
        isLoading: false,
        error: action.payload,
        refetch: false,
      };
    }
    default:
      return state;
  }
}

function addUserDetails(state = initialState, action) {
  switch (action.type) {
    case ADDUSER_API_REQUEST: {
      return { users: [], isLoading: true, error: null, refetch: false };
    }
    case ADDUSER_API_SUCCESS: {
      return {
        users: [action.payload],
        isLoading: false,
        error: null,
        refetch: true,
      };
    }
    case ADDUSER_API_FAILURE: {
      return {
        users: [],
        isLoading: false,
        error: action.payload,
        refetch: false,
      };
    }
    default:
      return state;
  }
}

function getUserById(state = initialState, action) {
  switch (action.type) {
    case ADDUSER_API_REQUEST: {
      return { users: [], isLoading: true, error: null };
    }
    case ADDUSER_API_SUCCESS: {
      return {
        users: [action.payload],
        isLoading: false,
        error: null,
      };
    }
    case ADDUSER_API_FAILURE: {
      return {
        users: [],
        isLoading: false,
        error: action.payload,
      };
    }
    default:
      return state;
  }
}

function editUser(state = initialState, action) {
  switch (action.type) {
    case ADDUSER_API_REQUEST: {
      return { users: [], isLoading: true, error: null };
    }
    case ADDUSER_API_SUCCESS: {
      return {
        users: [action.payload],
        isLoading: false,
        error: null,
      };
    }
    case ADDUSER_API_FAILURE: {
      return {
        users: [],
        isLoading: false,
        error: action.payload,
      };
    }
    default:
      return state;
  }
}

export { getUsers, deleteUser, addUserDetails, getUserById, editUser };
