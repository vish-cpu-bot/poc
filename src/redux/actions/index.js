import axios from "axios";
import {
  USERLIST_API_FAILURE,
  USERLIST_API_REQUEST,
  USERLIST_API_SUCCESS,
  USERDELETE_API_REQUEST,
  USERDELETE_API_SUCCESS,
  USERDELETE_API_FAILURE,
  ADDUSER_API_REQUEST,
  ADDUSER_API_SUCCESS,
  ADDUSER_API_FAILURE,
  EDITUSER_API_REQUEST,
  EDITUSER_API_SUCCESS,
  EDITUSER_API_FAILURE,
  GETUSER_API_REQUEST,
  GETUSER_API_SUCCESS,
  GETUSER_API_FAILURE,
} from "../../constant/constants";
import { API } from "../../config";
export const getUserList = () => {
  return async (dispatch) => {
    dispatch({ type: USERLIST_API_REQUEST, payload: "" });
    try {
      const result = await axios.get(API.userList);
      dispatch({ type: USERLIST_API_SUCCESS, payload: result.data });
    } catch (err) {
      dispatch({ type: USERLIST_API_FAILURE, payload: err });
      console.log(err);
    }
  };
};

export const deleteUser = (id) => {
  return async (dispatch) => {
    dispatch({ type: USERDELETE_API_REQUEST, payload: "" });
    try {
      const result = await axios.delete(`${API.deleteUser}/${id}`);
      dispatch({ type: USERDELETE_API_SUCCESS, payload: result.data });
    } catch (err) {
      dispatch({ type: USERDELETE_API_FAILURE, payload: err });
      console.log(err);
    }
  };
};

export const addUserDetails = (data) => {
  console.log("dataaa", data);
  return async (dispatch) => {
    dispatch({ type: ADDUSER_API_REQUEST, payload: "" });
    try {
      const result = await axios.post(`${API.addUser}`, data);
      dispatch({ type: ADDUSER_API_SUCCESS, payload: result.data });
    } catch (err) {
      dispatch({ type: ADDUSER_API_FAILURE, payload: err });
      console.log(err);
    }
  };
};
export const getUserById = (id) => {
  return async (dispatch) => {
    dispatch({ type: GETUSER_API_REQUEST, payload: "" });
    try {
      const result = await axios.get(`${API.addUser}/${id}`);
      dispatch({ type: GETUSER_API_SUCCESS, payload: result?.data });
    } catch (err) {
      dispatch({ type: GETUSER_API_FAILURE, payload: err });
      console.log(err);
    }
  };
};

export const editUserDetails = (id, data) => {
  console.log("idddddddddddddd", id, data);
  return async (dispatch) => {
    dispatch({ type: EDITUSER_API_REQUEST, payload: "" });
    try {
      const result = await axios.put(`${API.editUser}/${id}`, data);
      dispatch({ type: EDITUSER_API_SUCCESS, payload: result.data });
    } catch (err) {
      dispatch({ type: EDITUSER_API_FAILURE, payload: err });
      console.log(err);
    }
  };
};
