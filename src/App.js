import React from "react";
import { Route, Routes } from "react-router-dom";
import Landing from "./components/landing/landing";
import AddUser from "./components/adduser";
import EditUser from "./components/editUser";
import ViewUser from "./components/viewUser";

const App = () => {
  return (
    <>
      <Routes>
        <Route path="/" element={<Landing />} />
        <Route exact path="/adduser" element={<AddUser />} />
        <Route exact path="/edituser/:id" element={<EditUser />} />
        <Route exact path="/viewuser/:id" element={<ViewUser />} />
      </Routes>
    </>
  );
};

export default App;
