import React, { useState } from "react";
import AppBar from "@mui/material/AppBar";
import Box from "@mui/material/Box";
import Toolbar from "@mui/material/Toolbar";
import Typography from "@mui/material/Typography";
import Button from "@mui/material/Button";
import { useNavigate } from "react-router-dom";
import { TextField } from "@mui/material";

const Header = ({ chooseSearchKey }) => {
  const [searchData, setSearchData] = useState();
  const navigate = useNavigate();
  const onClickHandler = () => {
    navigate("/adduser");
  };

  const result = window.location.pathname;
  const onSearchChange = () => {};
  return (
    <Box sx={{ flexGrow: 1 }}>
      <AppBar position="static" style={{ backgroundColor: "#E5E4E2" }}>
        <Toolbar>
          <Typography
            color={"black"}
            variant="h6"
            component="div"
            sx={{ flexGrow: 1 }}
          >
            POC Group-3
          </Typography>
          <TextField
            id="outlined-basic"
            size="small"
            variant="outlined"
            type="search"
            onChange={(e) => chooseSearchKey(e?.target?.value)}
            style={{ marginRight: "8px" }}
          />
          {result === "/" ? (
            <Button onClick={() => onClickHandler()} variant="outlined">
              Add User
            </Button>
          ) : (
            ""
          )}
        </Toolbar>
      </AppBar>
    </Box>
  );
};

export default Header;
