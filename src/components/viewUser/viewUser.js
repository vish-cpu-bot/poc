import React, { useState } from "react";
import Box from "@mui/material/Box";
import TextField from "@mui/material/TextField";
import Button from "@mui/material/Button";
import Stack from "@mui/material/Stack";
import { useNavigate, useParams } from "react-router-dom";
import Header from "../header";
import { useDispatch, useSelector } from "react-redux";
import { getUserById } from "../../redux/actions";

function ViewUser() {
  const getUserDetailsById = useSelector(
    (state) => state?.getUserById?.users[0]
  );

  const [basicDetails, setBasicDetails] = useState({
    first_name: getUserDetailsById?.first_name,
    last_name: getUserDetailsById?.last_name,
    mobile_no: getUserDetailsById?.mobile_no,
    email: getUserDetailsById?.email,
    gender: getUserDetailsById?.gender,
    location: getUserDetailsById?.location,
    city: getUserDetailsById?.city,
    state: getUserDetailsById?.state,
    zip: getUserDetailsById?.zip,
  });

  const [academicDetails, setAcademicDetails] = useState(
    getUserDetailsById?.academic_details?.length
      ? getUserDetailsById?.academic_details
      : [
          {
            course: "",
            college_name: "",
            completion_year: "",
            percentage: "",
          },
        ]
  );

  const [employmentDetails, setEmploymentDetails] = useState(
    getUserDetailsById?.employment_details?.length
      ? getUserDetailsById?.employment_details
      : [
          {
            organization_name: "",
            designation: "",
            date_of_joining: "",
            date_of_leaving: "",
          },
        ]
  );
  const navigate = useNavigate();
  const params = useParams();
  const { id } = params;
  const dispatch = useDispatch();
  React.useEffect(() => {
    dispatch(getUserById(id));
  }, [dispatch, id]);

  const {
    first_name,
    last_name,
    email,
    gender,
    mobile_no,
    location,
    city,
    state,
  } = basicDetails;

  const onClickBackHandle = () => {
    navigate("/");
  };

  return (
    <>
      <Header />
      <div
        style={{
          marginTop: "100",
          marginLeft: "50",
          marginRight: "50",
          padding: "50px",
        }}
      >
        <h1>Student Profile</h1>
        <Box>
          <Stack
            direction="row"
            spacing={2}
            style={{
              display: "flex",
              padding: "9px",
              flexDirection: "row",
              justifyContent: "end",
            }}
          >
            <Button variant="outlined" onClick={onClickBackHandle}>
              Back
            </Button>
          </Stack>
        </Box>
        <Box component="fieldset">
          <legend>Basic Information</legend>
          <Box
            component="form"
            sx={{
              "& > :not(style)": { m: 1, width: "25ch" },
            }}
            noValidate
            autoComplete="off"
          >
            <TextField
              id="outlined-basic"
              label="First Name"
              name="first_name"
              value={first_name}
              type="text"
              variant="outlined"
              disabled={true}
            />
            <TextField
              id="outlined-basic"
              label="Last Name"
              name="last_name"
              value={last_name}
              type="text"
              variant="outlined"
              disabled={true}
            />
            <TextField
              id="outlined-basic"
              label="Email"
              name="email"
              value={email}
              type="email"
              variant="outlined"
              disabled={true}
            />
            <TextField
              id="outlined-basic"
              label="Gender"
              name="gender"
              value={gender}
              type="text"
              variant="outlined"
              disabled={true}
            />
            <TextField
              id="outlined-basic"
              label="Phone Number"
              name="mobile_no"
              value={mobile_no}
              type="number"
              variant="outlined"
              disabled={true}
            />
            <TextField
              id="outlined-basic"
              label="Location"
              name="location"
              value={location}
              type="text"
              variant="outlined"
              disabled={true}
            />
            <TextField
              id="outlined-basic"
              label="City"
              name="city"
              value={city}
              type="text"
              variant="outlined"
              disabled={true}
            />
            <TextField
              id="outlined-basic"
              label="State"
              name="state"
              value={state}
              type="text"
              variant="outlined"
              disabled={true}
            />
          </Box>
        </Box>
        <br />
        <br />
        <Box component="fieldset">
          <legend>Academics Details</legend>
          {academicDetails?.map((element, index) => (
            <Box
              component="form"
              sx={{
                "& > :not(style)": { m: 1, width: "25ch" },
              }}
              noValidate
              autoComplete="off"
              key={index}
            >
              <TextField
                id="outlined-basic"
                label="Course"
                name="course"
                value={element?.course}
                type="text"
                variant="outlined"
                disabled={true}
              />
              <TextField
                id="outlined-basic"
                label="College Name"
                name="college_name"
                value={element?.college_name}
                type="text"
                variant="outlined"
                disabled={true}
              />
              <TextField
                id="outlined-basic"
                label="Completion Year"
                name="completion_year"
                value={element?.completion_year}
                type="text"
                variant="outlined"
                disabled={true}
              />
              <TextField
                id="outlined-basic"
                label="Percentage"
                name="percentage"
                value={element?.percentage}
                type="text"
                variant="outlined"
                disabled={true}
              />
            </Box>
          ))}
        </Box>
        <br />
        <br />

        <Box component="fieldset">
          <legend>Employment Details</legend>
          {employmentDetails?.map((emp, index) => (
            <Box
              component="form"
              sx={{
                "& > :not(style)": { m: 1, width: "25ch" },
              }}
              noValidate
              autoComplete="off"
              key={index}
            >
              <TextField
                id="outlined-basic"
                label="Organization Name"
                name="organization_name"
                value={emp?.organization_name}
                type="text"
                disabled={true}
                variant="outlined"
              />
              <TextField
                id="outlined-basic"
                label="Designation"
                name="designation"
                value={emp?.designation}
                type="text"
                disabled={true}
                variant="outlined"
              />
              <TextField
                id="outlined-basic"
                label="Date of Joining"
                name="date_of_joining"
                value={emp?.date_of_joining}
                type="date"
                disabled={true}
                variant="outlined"
              />
              <TextField
                id="outlined-basic"
                label="Date of Leaving"
                name="date_of_leaving"
                value={emp?.date_of_leaving}
                type="date"
                disabled={true}
                variant="outlined"
              />
            </Box>
          ))}
        </Box>
      </div>
    </>
  );
}

export default ViewUser;
