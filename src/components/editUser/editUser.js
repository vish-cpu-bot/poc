import React, { useState } from "react";
import Box from "@mui/material/Box";
import TextField from "@mui/material/TextField";
import Button from "@mui/material/Button";
import Stack from "@mui/material/Stack";
import { useNavigate, useParams } from "react-router-dom";
import Header from "../header";
import { red } from "@mui/material/colors";
import ClearOutlinedIcon from "@mui/icons-material/ClearOutlined";
import { useDispatch, useSelector } from "react-redux";
import { editUserDetails, getUserById } from "../../redux/actions";

function EditUser() {
  const getUserDetailsById = useSelector(
    (state) => state?.getUserById?.users[0]
  );

  const [basicDetails, setBasicDetails] = useState({
    first_name: getUserDetailsById?.first_name,
    last_name: getUserDetailsById?.last_name,
    mobile_no: getUserDetailsById?.mobile_no,
    email: getUserDetailsById?.email,
    gender: getUserDetailsById?.gender,
    location: getUserDetailsById?.location,
    city: getUserDetailsById?.city,
    state: getUserDetailsById?.state,
    zip: getUserDetailsById?.zip,
  });

  const [academicDetails, setAcademicDetails] = useState(
    getUserDetailsById?.academic_details?.length
      ? getUserDetailsById?.academic_details
      : [
          {
            course: "",
            college_name: "",
            completion_year: "",
            percentage: "",
          },
        ]
  );

  const [employmentDetails, setEmploymentDetails] = useState(
    getUserDetailsById?.employment_details?.length
      ? getUserDetailsById?.employment_details
      : [
          {
            organization_name: "",
            designation: "",
            date_of_joining: "",
            date_of_leaving: "",
          },
        ]
  );
  const navigate = useNavigate();
  const params = useParams();
  const { id } = params;
  const dispatch = useDispatch();

  React.useEffect(() => {
    dispatch(getUserById(id));
  }, [dispatch, id]);

  let addFormFieldsacdemics = () => {
    setAcademicDetails([
      ...academicDetails,
      {
        id: academicDetails?.length + 1,
        course: "",
        college_name: "",
        completion_year: "",
        percentage: "",
      },
    ]);
  };

  let removeFormFields = (i) => {
    let newFormValues = [...academicDetails];
    newFormValues.splice(i, 1);
    setAcademicDetails(newFormValues);
  };
  const handleInputChange = (e, index) => {
    let { name, value } = e.target;
    console.log("date", name, value);
    let tempAcademics = [...academicDetails];
    let tempEmployment = [...employmentDetails];
    tempAcademics[index] = { ...tempAcademics[index], [name]: value };
    tempEmployment[index] = { ...tempEmployment[index], [name]: value };
    setBasicDetails({ ...basicDetails, [name]: value });
    setAcademicDetails(tempAcademics);
    setEmploymentDetails(tempEmployment);
  };

  const finalSubmitHandler = () => {
    dispatch(
      editUserDetails(id, {
        ...basicDetails,
        academic_details: academicDetails,
        employment_details: employmentDetails,
      })
    );
    navigate("/");
  };

  const onClickBackHandle = () => {
    navigate("/");
  };

  const {
    first_name,
    last_name,
    email,
    gender,
    mobile_no,
    location,
    city,
    state,
  } = basicDetails;

  return (
    <>
      <Header />
      <div
        style={{
          marginTop: "100",
          marginLeft: "50",
          marginRight: "50",
          padding: "50px",
        }}
      >
        <h1>Student Profile</h1>
        <Box>
          <Stack
            direction="row"
            spacing={2}
            style={{
              display: "flex",
              padding: "9px",
              flexDirection: "row",
              justifyContent: "end",
            }}
          >
            <Button variant="outlined" onClick={onClickBackHandle}>
              Back
            </Button>
          </Stack>
        </Box>
        <Box component="fieldset">
          <legend>Basic Information</legend>
          <Box
            component="form"
            sx={{
              "& > :not(style)": { m: 1, width: "25ch" },
            }}
            noValidate
            autoComplete="off"
          >
            <TextField
              id="outlined-basic"
              label="First Name"
              name="first_name"
              onChange={handleInputChange}
              value={first_name}
              type="text"
              variant="outlined"
            />
            <TextField
              id="outlined-basic"
              label="Last Name"
              name="last_name"
              onChange={handleInputChange}
              value={last_name}
              type="text"
              variant="outlined"
            />
            <TextField
              id="outlined-basic"
              label="Email"
              name="email"
              onChange={handleInputChange}
              value={email}
              type="email"
              variant="outlined"
            />
            <TextField
              id="outlined-basic"
              label="Gender"
              name="gender"
              onChange={handleInputChange}
              value={gender}
              type="text"
              variant="outlined"
            />
            <TextField
              id="outlined-basic"
              label="Phone Number"
              name="mobile_no"
              onChange={handleInputChange}
              value={mobile_no}
              type="number"
              variant="outlined"
            />
            <TextField
              id="outlined-basic"
              label="Location"
              name="location"
              onChange={handleInputChange}
              value={location}
              type="text"
              variant="outlined"
            />
            <TextField
              id="outlined-basic"
              label="City"
              name="city"
              onChange={handleInputChange}
              value={city}
              type="text"
              variant="outlined"
            />
            <TextField
              id="outlined-basic"
              label="State"
              name="state"
              onChange={handleInputChange}
              value={state}
              type="text"
              variant="outlined"
            />
          </Box>
        </Box>
        <br />
        <br />
        <Box component="fieldset">
          <legend>Academics Details</legend>
          {academicDetails?.map((element, index) => (
            <Box
              component="form"
              sx={{
                "& > :not(style)": { m: 1, width: "25ch" },
              }}
              noValidate
              autoComplete="off"
              key={index}
            >
              <TextField
                id="outlined-basic"
                label="Course"
                name="course"
                onChange={(e) => handleInputChange(e, index)}
                value={element?.course}
                type="text"
                variant="outlined"
              />
              <TextField
                id="outlined-basic"
                label="College Name"
                name="college_name"
                onChange={(e) => handleInputChange(e, index)}
                value={element?.college_name}
                type="text"
                variant="outlined"
              />
              <TextField
                id="outlined-basic"
                label="Completion Year"
                name="completion_year"
                onChange={(e) => handleInputChange(e, index)}
                value={element?.completion_year}
                type="text"
                variant="outlined"
              />
              <TextField
                id="outlined-basic"
                label="Percentage"
                name="percentage"
                onChange={(e) => handleInputChange(e, index)}
                value={element?.percentage}
                type="text"
                variant="outlined"
              />
              {index ? (
                <ClearOutlinedIcon
                  style={{ cursor: "pointer", marginRight: "2px" }}
                  onClick={() => removeFormFields(index)}
                  sx={{ color: red[700] }}
                />
              ) : null}
            </Box>
          ))}
          <Stack
            direction="row"
            spacing={2}
            style={{
              display: "flex",
              padding: "9px",
              flexDirection: "row",
              justifyContent: "start",
            }}
          >
            <Button variant="outlined" onClick={() => addFormFieldsacdemics()}>
              Add
            </Button>
          </Stack>
        </Box>
        <br />
        <br />

        <Box component="fieldset">
          <legend>Employment Details</legend>
          {employmentDetails?.map((emp, index) => (
            <Box
              component="form"
              sx={{
                "& > :not(style)": { m: 1, width: "25ch" },
              }}
              noValidate
              autoComplete="off"
              key={index}
            >
              <TextField
                id="outlined-basic"
                label="Organization Name"
                name="organization_name"
                onChange={(e) => handleInputChange(e, index)}
                value={emp?.organization_name}
                type="text"
                variant="outlined"
              />
              <TextField
                id="outlined-basic"
                label="Designation"
                name="designation"
                onChange={(e) => handleInputChange(e, index)}
                value={emp?.designation}
                type="text"
                variant="outlined"
              />
              <TextField
                id="outlined-basic"
                label="Date of Joining"
                name="date_of_joining"
                onChange={(e) => handleInputChange(e, index)}
                value={emp?.date_of_joining}
                type="date"
                variant="outlined"
              />
              <TextField
                id="outlined-basic"
                label="Date of Leaving"
                name="date_of_leaving"
                onChange={(e) => handleInputChange(e, index)}
                value={emp?.date_of_leaving}
                type="date"
                variant="outlined"
              />
            </Box>
          ))}
        </Box>
        <Button
          style={{ marginTop: "8px", float: "right" }}
          variant="outlined"
          onClick={() => finalSubmitHandler()}
        >
          Save
        </Button>
      </div>
    </>
  );
}

export default EditUser;
