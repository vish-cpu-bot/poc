import React, { useState } from "react";
import Box from "@mui/material/Box";
import TextField from "@mui/material/TextField";
import Button from "@mui/material/Button";
import Stack from "@mui/material/Stack";
import dayjs from "dayjs";
import { useNavigate } from "react-router-dom";
import Header from "../header";
import { red } from "@mui/material/colors";
import ClearOutlinedIcon from "@mui/icons-material/ClearOutlined";
import { useDispatch } from "react-redux";
import { addUserDetails } from "../../redux/actions";
import InputLabel from "@mui/material/InputLabel";
import MenuItem from "@mui/material/MenuItem";
import Select from "@mui/material/Select";
import FormControl from "@mui/material/FormControl";
import Alert from "@mui/material/Alert";
import AlertTitle from "@mui/material/AlertTitle";
import { DatePicker } from "@mui/x-date-pickers/DatePicker";
import { LocalizationProvider } from "@mui/x-date-pickers/LocalizationProvider";
import { AdapterDayjs } from "@mui/x-date-pickers/AdapterDayjs";
import { display } from "@mui/system";
import { DateTimePicker } from "@mui/x-date-pickers";

function AddUser() {
  const regex_text = /^[a-zA-Z]+$/;
  const regex_mob = /^(\+\d{1,3}[- ]?)?\d{10}$/;
  const regex_email =
    /^(([^<>()[\]\.,;:\s@\"]+(\.[^<>()[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;

  const navigate = useNavigate();

  const onClickBackHandle = () => {
    navigate("/");
  };

  const [basicDetails, setBasicDetails] = useState({
    first_name: "",
    last_name: "",
    mobile_no: "",
    email: "",
    gender: "",
    location: "",
    city: "",
    state: "",
    zip: "",
  });

  const [academicDetails, setAcademicDetails] = useState([
    {
      course: "",
      college_name: "",
      completion_year: "",
      percentage: "",
    },
  ]);

  const [employmentDetails, setEmploymentDetails] = useState({
    organization_name: "",
    designation: "",
    date_of_joining: "",
    date_of_leaving: "",
  });

  const [hasValidFirstname, setHasValidFirstname] = React.useState(true);
  const [hasValidLastname, setHasValidLastname] = React.useState(true);
  const [hasValidEmail, setHasValidEmail] = React.useState(true);
  const [hasValidMobile, setHasValidMobile] = React.useState(true);

  const dispatch = useDispatch();

  let addFormFieldsacdemics = () => {
    setAcademicDetails([
      ...academicDetails,
      {
        course: "",
        college_name: "",
        completion_year: "",
        percentage: "",
      },
    ]);
  };

  let removeFormFields = (i) => {
    let newFormValues = [...academicDetails];
    newFormValues.splice(i, 1);
    setAcademicDetails(newFormValues);
  };

  const handleInputChange = (e, index) => {
    let { name, value } = e.target;
    let tempAcademics = [...academicDetails];
    tempAcademics[index] = { ...tempAcademics[index], [name]: value };

    setBasicDetails({ ...basicDetails, [name]: value });
    setAcademicDetails(tempAcademics);
    setEmploymentDetails({ ...employmentDetails, [name]: value });

    if (name === "first_name") {
      if (!value?.length || regex_text.test(value) === false) {
        setHasValidFirstname(false);
      } else {
        setHasValidFirstname(true);
      }
    }

    if (name === "last_name") {
      if (!value?.length || regex_text.test(value) === false) {
        setHasValidLastname(false);
      } else {
        setHasValidLastname(true);
      }
    }
    if (name === "email") {
      if (!value?.length || regex_email.test(value) === false) {
        setHasValidEmail(false);
      } else {
        setHasValidEmail(true);
      }
    }
    if (name === "mobile_no") {
      if (value?.length < 10 || regex_mob.test(value) === false) {
        setHasValidMobile(false);
      } else {
        setHasValidMobile(true);
      }
    }
  };

  const finalSubmitHandler = (e) => {
    e.preventDefault();
    console.log("hello");
    const allValidated =
      basicDetails.first_name?.length > 0 &&
      regex_text.test(basicDetails.first_name) &&
      basicDetails.last_name?.length > 0 &&
      regex_text.test(basicDetails.last_name) &&
      basicDetails.email?.length > 0 &&
      regex_email.test(basicDetails.email) &&
      basicDetails.mobile_no?.length > 0 &&
      regex_mob.test(basicDetails.mobile_no);

    if (
      !basicDetails.first_name?.length ||
      regex_text.test(basicDetails.first_name) === false
    ) {
      setHasValidFirstname(false);
    } else {
      setHasValidFirstname(true);
    }
    if (
      !basicDetails.last_name?.length ||
      regex_text.test(basicDetails.last_name) === false
    ) {
      setHasValidLastname(false);
    } else {
      setHasValidLastname(true);
    }
    if (
      !basicDetails.email?.length ||
      regex_email.test(basicDetails.email) === false
    ) {
      setHasValidEmail(false);
    } else {
      setHasValidEmail(true);
    }
    if (
      basicDetails.mobile_no?.length < 10 ||
      regex_mob.test(basicDetails.mobile_no) === false
    ) {
      setHasValidMobile(false);
    } else {
      setHasValidMobile(true);
    }
    if (allValidated) {
      dispatch(
        addUserDetails({
          ...basicDetails,
          academic_details: academicDetails,
          employment_details: [{ ...employmentDetails }],
        })
      );
      navigate("/");
    }

    <Alert severity="success">
      <AlertTitle>Success</AlertTitle>
      This is a success alert — <strong>check it out!</strong>
    </Alert>;
  };

  const {
    first_name,
    last_name,
    mobile_no,
    email,
    gender,
    location,
    city,
    state,
    zip,
  } = basicDetails;
  const { course, college_name, completion_year, percentage } = academicDetails;
  const { organization_name, designation, date_of_joining, date_of_leaving } =
    employmentDetails;

  const today = dayjs();
  const tomorrow = dayjs().add(1, "day");
  const todayEndOfTheDay = today.endOf("day");

  const setEmpLocal = () => {
    localStorage.setItem("employment", JSON.stringify(employmentDetails));
  };
  const setEmpAcademic = () => {
    localStorage.setItem("academic", JSON.stringify(academicDetails));
  };
  const handleBasicLocal = () => {
    localStorage.setItem("basic", JSON.stringify(basicDetails));
  };
  return (
    <>
      <Header />
      <div
        style={{
          marginTop: "100",
          marginLeft: "50",
          marginRight: "50",
          padding: "50px",
        }}
      >
        <LocalizationProvider dateAdapter={AdapterDayjs}>
          <Box>
            <Stack
              direction="row"
              spacing={2}
              style={{
                display: "flex",
                padding: "9px",
                flexDirection: "row",
                justifyContent: "end",
              }}
            >
              <Button variant="outlined" onClick={onClickBackHandle}>
                Back
              </Button>
            </Stack>
          </Box>

          <h1>Student Profile</h1>
          <Box component="form" noValidate autoComplete="off">
            <Box component="fieldset">
              <legend>Basic Information</legend>
              <Box
                sx={{
                  "& > :not(style)": { m: 1, width: "25ch" },
                }}
              >
                <TextField
                  id="outlined-basic"
                  label="First Name"
                  name="first_name"
                  required
                  onChange={handleInputChange}
                  value={first_name}
                  type="text"
                  variant="outlined"
                  error={!hasValidFirstname}
                  helperText={
                    !hasValidFirstname ? "please enter valid text" : ""
                  }
                />
                <TextField
                  id="outlined-basic"
                  label="Last Name"
                  name="last_name"
                  required
                  onChange={handleInputChange}
                  value={last_name}
                  type="text"
                  variant="outlined"
                  error={!hasValidLastname}
                  helperText={
                    !hasValidLastname ? "please enter valid text" : ""
                  }
                />
                <TextField
                  id="outlined-basic"
                  label="Email"
                  name="email"
                  onChange={handleInputChange}
                  value={email}
                  type="email"
                  required
                  variant="outlined"
                  error={!hasValidEmail}
                  helperText={!hasValidEmail ? "please enter valid email" : ""}
                />
                <FormControl>
                  <InputLabel id="demo-simple-select-label">Gender</InputLabel>
                  <Select
                    labelId="demo-simple-select-label"
                    id="demo-simple-select"
                    value={gender}
                    name={"gender"}
                    label="Gender"
                    onChange={handleInputChange}
                  >
                    <MenuItem value={""}>Select </MenuItem>
                    <MenuItem value={"male"}>Male</MenuItem>
                    <MenuItem value={"female"}>Female</MenuItem>
                    <MenuItem value={"other"}>Other</MenuItem>
                  </Select>
                </FormControl>
                <TextField
                  id="outlined-basic"
                  label="Phone Number"
                  name="mobile_no"
                  onChange={handleInputChange}
                  value={mobile_no}
                  type="number"
                  required
                  variant="outlined"
                  error={!hasValidMobile}
                  helperText={
                    !hasValidMobile ? "please enter valid mobile number" : ""
                  }
                />
                <TextField
                  id="outlined-basic"
                  label="Location"
                  name="location"
                  onChange={handleInputChange}
                  value={location}
                  type="text"
                  variant="outlined"
                />
                <TextField
                  id="outlined-basic"
                  label="City"
                  name="city"
                  onChange={handleInputChange}
                  value={city}
                  type="text"
                  variant="outlined"
                />
                <TextField
                  id="outlined-basic"
                  label="State"
                  name="state"
                  onChange={handleInputChange}
                  value={state}
                  type="text"
                  variant="outlined"
                />
                <TextField
                  id="outlined-basic"
                  label="Zip"
                  name="zip"
                  onChange={handleInputChange}
                  value={zip}
                  type="text"
                  variant="outlined"
                />
                <br />
              </Box>
              <Button
                onClick={() => handleBasicLocal()}
                variant="outlined"
                style={{ float: "right" }}
              >
                Save
              </Button>
            </Box>
            <br />
            <br />
            <Box component="fieldset">
              <legend>Academics Details</legend>
              {academicDetails?.map((element, index) => (
                <Box
                  component="form"
                  sx={{
                    "& > :not(style)": { m: 1, width: "25ch" },
                  }}
                  noValidate
                  autoComplete="off"
                  key={index}
                >
                  <TextField
                    id="outlined-basic"
                    label="Course"
                    name="course"
                    onChange={(e) => handleInputChange(e, index)}
                    value={course}
                    type="text"
                    variant="outlined"
                  />
                  <TextField
                    id="outlined-basic"
                    label="College Name"
                    name="college_name"
                    onChange={(e) => handleInputChange(e, index)}
                    value={college_name}
                    type="text"
                    variant="outlined"
                  />
                  <TextField
                    id="outlined-basic"
                    label="Completion Year"
                    name="completion_year"
                    onChange={(e) => handleInputChange(e, index)}
                    value={completion_year}
                    type="text"
                    variant="outlined"
                  />
                  <TextField
                    id="outlined-basic"
                    label="Percentage"
                    name="percentage"
                    onChange={(e) => handleInputChange(e, index)}
                    value={percentage}
                    type="text"
                    variant="outlined"
                  />
                  {index ? (
                    <ClearOutlinedIcon
                      style={{ cursor: "pointer", marginRight: "2px" }}
                      onClick={() => removeFormFields(index)}
                      sx={{ color: red[700] }}
                    />
                  ) : null}
                </Box>
              ))}
              <Stack
                direction="row"
                spacing={2}
                style={{
                  display: "flex",
                  padding: "9px",
                  flexDirection: "row",
                  justifyContent: "start",
                }}
              >
                <Button
                  variant="outlined"
                  onClick={() => addFormFieldsacdemics()}
                >
                  Add
                </Button>
              </Stack>
              <Button
                onClick={() => setEmpAcademic()}
                variant="outlined"
                style={{ float: "right" }}
              >
                Save
              </Button>
            </Box>
            <br />
            <br />
            <Box component="fieldset">
              <legend>Employment Details</legend>
              <Box
                component="form"
                sx={{
                  "& > :not(style)": { m: 1, width: "25ch" },
                }}
                noValidate
                autoComplete="off"
              >
                <TextField
                  id="outlined-basic"
                  label="Organization Name"
                  name="organization_name"
                  onChange={handleInputChange}
                  value={organization_name}
                  type="text"
                  variant="outlined"
                />
                <TextField
                  id="outlined-basic"
                  label="Designation"
                  name="designation"
                  onChange={handleInputChange}
                  value={designation}
                  type="text"
                  variant="outlined"
                />
                <TextField
                  id="outlined-basic"
                  label="Date of Joining"
                  name="date_of_joining"
                  onChange={handleInputChange}
                  value={date_of_joining}
                  type="date"
                  min={new Date().toJSON().slice(0, 10)}
                  variant="outlined"
                  InputLabelProps={{ shrink: true }}
                />
                {/* <TextField
                  id="outlined-basic"
                  label="Date Of Leaving"
                  name="date_of_leaving"
                  onChange={handleInputChange}
                  value={date_of_leaving}
                  type="date"
                  disableFuture={true}
                  variant="outlined"
                  InputLabelProps={{ shrink: true }}
                /> */}
                <DateTimePicker
                  defaultValue={tomorrow}
                  disableFuture
                  value={date_of_leaving}
                  name="date_of_leaving"
                  views={["year", "month", "day", "hours", "minutes"]}
                />
                {/* <DatePicker
                  defaultValue={tomorrow}
                  value={date_of_leaving}
                  name="date_of_leaving"
                  disableFuture
                  views={["year", "month", "day"]}
                /> */}
              </Box>
              <Button
                onClick={() => setEmpLocal()}
                variant="outlined"
                style={{ float: "right" }}
              >
                Save
              </Button>
            </Box>
            <Button
              style={{ marginTop: "8px", float: "right" }}
              variant="outlined"
              // type="submit"
              onClick={(e) => finalSubmitHandler(e)}
            >
              Save
            </Button>
          </Box>
        </LocalizationProvider>
      </div>
    </>
  );
}

export default AddUser;
