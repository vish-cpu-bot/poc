import React, { useState } from "react";
import Box from "@mui/material/Box";
import TextField from "@mui/material/TextField";
import Button from "@mui/material/Button";
import Stack from "@mui/material/Stack";
import { useNavigate } from "react-router-dom";
import Header from "../header";
import { red } from "@mui/material/colors";
import ClearOutlinedIcon from "@mui/icons-material/ClearOutlined";
import { useDispatch } from "react-redux";
import { addUserDetails } from "../../redux/actions";

function AddUser() {
  const navigate = useNavigate();

  const handleSubmit = (e) => {
    e.preventDefault();
  };

  const onClickBackHandle = () => {
    navigate("/");
  };

  const [addUser, setaddUser] = useState({
    first_name: "",
    last_name: "",
    mobile_no: " ",
    email: "",
    gender: "",
    location: "",
    city: "",
    state: "",
    zip: "",
  });

  const [academics, setAcademics] = useState([
    {
      id: 1,
      course: "",
      college_name: "",
      completion_year: " ",
      percentage: "",
    },
  ]);

  const [employment, setEmployment] = useState({
    organization_name: "",
    designation: "",
    completion_year: " ",
    percentage: "",
  });
  const dispatch = useDispatch();

  let addFormFieldsacdemics = () => {
    setAcademics([
      ...academics,
      {
        id: academics?.length + 1,
        course: "",
        college_name: "",
        completion_year: " ",
        percentage: "",
      },
    ]);
  };

  let removeFormFields = (i) => {
    let newFormValues = [...academics];
    newFormValues.splice(i, 1);
    setAcademics(newFormValues);
  };
  const handleInputChange = (e, id) => {
    let { name, value } = e.target;
    let tempAcademics = [...academics];
    const index = tempAcademics?.findIndex((row) => row.id === id);
    tempAcademics[index] = { ...tempAcademics[index], [name]: value };
    setaddUser({ ...addUser, [name]: value });
    setAcademics(tempAcademics);
    setEmployment({ ...employment, [name]: value });
  };

  const {
    first_name,
    last_name,
    email,
    gender,
    mobile_no,
    location,
    city,
    state,
  } = addUser;
  const { course, college_name, completion_year, percentage } = academics;
  const { organization_name, designation, date_of_joining, date_of_leaving } =
    employment;

  const finalSubmitHandler = () => {
    dispatch(
      addUserDetails({
        ...addUser,
        academic_details: academics,
        employment_details: [{ ...employment }],
      })
    );
    navigate("/");
  };

  return (
    <>
      <Header />
      <div
        style={{
          marginTop: "100",
          marginLeft: "50",
          marginRight: "50",
          padding: "50px",
        }}
      >
        <h1>Student Profile</h1>
        <Box>
          <Stack
            direction="row"
            spacing={2}
            style={{
              display: "flex",
              padding: "9px",
              flexDirection: "row",
              justifyContent: "end",
            }}
          >
            <Button variant="outlined" onClick={onClickBackHandle}>
              Back
            </Button>
          </Stack>
        </Box>
        <Box component="fieldset">
          <legend>Basic Information</legend>
          <Box
            component="form"
            sx={{
              "& > :not(style)": { m: 1, width: "25ch" },
            }}
            noValidate
            autoComplete="off"
            // onSubmit={handleSubmit}
          >
            <TextField
              id="outlined-basic"
              label="First Name"
              name="first_name"
              onChange={handleInputChange}
              value={first_name}
              type="text"
              variant="outlined"
            />
            <TextField
              id="outlined-basic"
              label="Last Name"
              name="last_name"
              onChange={handleInputChange}
              value={last_name}
              type="text"
              variant="outlined"
            />
            <TextField
              id="outlined-basic"
              label="Email"
              name="email"
              onChange={handleInputChange}
              value={email}
              type="email"
              variant="outlined"
            />
            <TextField
              id="outlined-basic"
              label="Gender"
              name="gender"
              onChange={handleInputChange}
              value={gender}
              type="text"
              variant="outlined"
            />
            <TextField
              id="outlined-basic"
              label="Phone Number"
              name="mobile_no"
              onChange={handleInputChange}
              value={mobile_no}
              type="number"
              variant="outlined"
            />
            <TextField
              id="outlined-basic"
              label="Location"
              name="location"
              onChange={handleInputChange}
              value={location}
              type="text"
              variant="outlined"
            />
            <TextField
              id="outlined-basic"
              label="City"
              name="city"
              onChange={handleInputChange}
              value={city}
              type="text"
              variant="outlined"
            />
            <TextField
              id="outlined-basic"
              label="State"
              name="state"
              onChange={handleInputChange}
              value={state}
              type="text"
              variant="outlined"
            />
            <br />
          </Box>
          <Stack
            direction="row"
            spacing={2}
            style={{
              display: "flex",
              padding: "9px",
              flexDirection: "row",
              justifyContent: "end",
            }}
          ></Stack>
        </Box>
        <br />
        <br />
        <Box component="fieldset">
          <legend>Academics Details</legend>
          {academics?.map((element, index) => (
            <Box
              component="form"
              sx={{
                "& > :not(style)": { m: 1, width: "25ch" },
              }}
              noValidate
              autoComplete="off"
              onSubmit={handleSubmit}
              key={element?.id}
            >
              <TextField
                id="outlined-basic"
                label="Course"
                name="course"
                onChange={(e) => handleInputChange(e, element?.id)}
                value={course}
                type="text"
                variant="outlined"
              />
              <TextField
                id="outlined-basic"
                label="College Name"
                name="college_name"
                onChange={(e) => handleInputChange(e, element?.id)}
                value={college_name}
                type="text"
                variant="outlined"
              />
              <TextField
                id="outlined-basic"
                label="Completion Year"
                name="completion_year"
                onChange={(e) => handleInputChange(e, element?.id)}
                value={completion_year}
                type="text"
                variant="outlined"
              />

              <TextField
                id="outlined-basic"
                label="Percentage"
                name="percentage"
                onChange={(e) => handleInputChange(e, element?.id)}
                value={percentage}
                type="text"
                variant="outlined"
              />
              {index ? (
                <ClearOutlinedIcon
                  style={{ cursor: "pointer", marginRight: "2px" }}
                  onClick={() => removeFormFields(index)}
                  sx={{ color: red[700] }}
                />
              ) : null}
            </Box>
          ))}
          <Stack
            direction="row"
            spacing={2}
            style={{
              display: "flex",
              padding: "9px",
              flexDirection: "row",
              justifyContent: "start",
            }}
          >
            <Button variant="outlined" onClick={() => addFormFieldsacdemics()}>
              Add
            </Button>
          </Stack>

          <Stack
            direction="row"
            spacing={2}
            style={{
              display: "flex",
              padding: "9px",
              flexDirection: "row",
              justifyContent: "end",
            }}
          >
            {/* <Button variant="outlined" onClick={() => addAcademicDetails()}>
              Continue
            </Button> */}
          </Stack>
        </Box>
        <br />
        <br />

        <Box component="fieldset">
          <legend>Employment Details</legend>
          <Box
            component="form"
            sx={{
              "& > :not(style)": { m: 1, width: "25ch" },
            }}
            noValidate
            autoComplete="off"
          >
            <TextField
              id="outlined-basic"
              label="Organization Name"
              name="organization_name"
              onChange={handleInputChange}
              value={organization_name}
              type="text"
              variant="outlined"
            />
            <TextField
              id="outlined-basic"
              label="Designation"
              name="designation"
              onChange={handleInputChange}
              value={designation}
              type="text"
              variant="outlined"
            />
            <TextField
              id="outlined-basic"
              label="Date of Joining"
              name="date_of_joining"
              onChange={handleInputChange}
              value={date_of_joining}
              type="text"
              variant="outlined"
            />
            <TextField
              id="outlined-basic"
              label="Percentage"
              name="date_of_leaving"
              onChange={handleInputChange}
              value={date_of_leaving}
              type="text"
              variant="outlined"
            />

            <br />
          </Box>
          <Stack
            direction="row"
            spacing={2}
            style={{
              display: "flex",
              padding: "9px",
              flexDirection: "row",
              justifyContent: "end",
            }}
          >
            {/* <Button
              variant="outlined"
              onClick={() => {
                addEmploymentDetails();
              }}
            >
              Countinue
            </Button> */}
          </Stack>
        </Box>
        <Button
          style={{ marginTop: "8px", float: "right" }}
          variant="outlined"
          onClick={() => finalSubmitHandler()}
        >
          Save
        </Button>
      </div>
    </>
  );
}

export default AddUser;
