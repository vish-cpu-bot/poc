import * as React from "react";
import Paper from "@mui/material/Paper";
import Table from "@mui/material/Table";
import TableBody from "@mui/material/TableBody";
import TableCell from "@mui/material/TableCell";
import TableContainer from "@mui/material/TableContainer";
import TableHead from "@mui/material/TableHead";
import TablePagination from "@mui/material/TablePagination";
import TableRow from "@mui/material/TableRow";
import { Container } from "@mui/system";
import { Box, Grid } from "@mui/material";
import { getUserById, getUserList } from "../../redux/actions";
import DeleteIcon from "@mui/icons-material/Delete";
import { red } from "@mui/material/colors";
import EditIcon from "@mui/icons-material/Edit";
import DeleteModal from "./modal";
import RemoveRedEyeOutlinedIcon from "@mui/icons-material/RemoveRedEyeOutlined";
import { useNavigate } from "react-router-dom";
import Header from "../header";
import { useDispatch, useSelector } from "react-redux";
import ArrowUpwardOutlinedIcon from "@mui/icons-material/ArrowUpwardOutlined";
import SouthOutlinedIcon from "@mui/icons-material/SouthOutlined";

function createData(Firstname, LastName, Email, Mobile, Gender) {
  return { Firstname, LastName, Email, Mobile, Gender };
}
const rows = [
  createData("vishal", "dogra", "test@yopmail.com", "Male", 1234567890),
  createData("vishal", "dogra", "test@yopmail.com", "Male", 1234567890),
];

const Landing = () => {
  const [page, setPage] = React.useState(0);
  const [deleteModal, setDelteModal] = React.useState(false);
  const [rowsPerPage, setRowsPerPage] = React.useState(10);
  const [deleteId, setDeleteId] = React.useState();
  const [globalStateData, setGlobalStateData] = React.useState([]);
  const dispatch = useDispatch();
  const userDetails = useSelector((state) => state?.getUsers?.users[0]);
  const deleteUser = useSelector((state) => state?.deleteUser?.refetch);
  const [searchedData, setSearchedData] = React.useState("");
  const [sortBy, setSortby] = React.useState({
    first_name: false,
    last_name: false,
    email: false,
    mobile_no: false,
    gender: false,
  });
  React.useEffect(() => {
    setGlobalStateData(userDetails);
  }, [userDetails]);
  React.useEffect(() => {
    dispatch(getUserList());
  }, [dispatch, deleteUser]);

  const navigate = useNavigate();
  const onEditHandler = (id) => {
    dispatch(getUserById(id));
    setTimeout(() => {
      navigate(`/edituser/${id}`);
    }, 1000);
  };

  const onViewHandler = (id) => {
    dispatch(getUserById(id));
    setTimeout(() => {
      navigate(`/viewuser/${id}`);
    }, 1000);
  };

  const handleChangePage = (event, newPage) => {
    setPage(newPage);
  };

  const handleChangeRowsPerPage = (event) => {
    setRowsPerPage(+event.target.value);
    setPage(0);
  };

  const onDeleteUser = (id) => {
    setDeleteId(id);
    setDelteModal(true);
  };

  const handleSort = (column) => {
    switch (column) {
      case "first_name":
        if (sortBy?.first_name) {
          setGlobalStateData(
            globalStateData?.sort((a, b) =>
              a?.first_name.localeCompare(b.first_name)
            )
          );
        } else {
          setGlobalStateData(
            globalStateData?.sort((a, b) =>
              b?.first_name.localeCompare(a.first_name)
            )
          );
        }

        break;
      case "last_name":
        if (sortBy?.last_name) {
          setGlobalStateData(
            globalStateData?.sort((a, b) =>
              a?.last_name.localeCompare(b.last_name)
            )
          );
        } else {
          setGlobalStateData(
            globalStateData?.sort((a, b) =>
              b?.last_name.localeCompare(a.last_name)
            )
          );
        }
        break;
      case "mobile_no":
        if (sortBy?.mobile_no) {
          setGlobalStateData(
            globalStateData?.sort((a, b) => +a?.mobile_no - +b.mobile_no)
          );
        } else {
          setGlobalStateData(
            globalStateData?.sort((a, b) => +b?.mobile_no - +a.mobile_no)
          );
        }
        break;
      case "gender":
        if (sortBy?.gender) {
          setGlobalStateData(
            globalStateData?.sort((a, b) => a?.gender.localeCompare(b.gender))
          );
        } else {
          setGlobalStateData(
            globalStateData?.sort((a, b) => a?.gender.localeCompare(b.gender))
          );
        }
        break;
      case "email":
        if (sortBy?.email) {
          setGlobalStateData(
            globalStateData?.sort((a, b) => a?.email.localeCompare(b.email))
          );
        } else {
          setGlobalStateData(
            globalStateData?.sort((a, b) => a?.email.localeCompare(b.email))
          );
        }
        break;

      default:
        break;
    }
  };

  const chooseSearchKey = (msg) => {
    setSearchedData(msg);
  };
  return (
    <>
      <Header chooseSearchKey={chooseSearchKey} />
      <Container style={{ marginTop: "1rem" }}>
        <Grid item>
          <TableContainer component={Paper}>
            <Table sx={{ minWidth: 650 }} aria-label="simple table">
              <TableHead>
                <TableRow>
                  <TableCell align="center" style={{ fontWeight: "bold" }}>
                    First Name
                    <span
                      onClick={() => {
                        setSortby({
                          ...sortBy,
                          first_name: !sortBy?.first_name,
                        });
                        handleSort("first_name");
                      }}
                    >
                      {sortBy?.first_name ? (
                        <ArrowUpwardOutlinedIcon fontSize="16" />
                      ) : (
                        <SouthOutlinedIcon fontSize="16" />
                      )}
                    </span>
                  </TableCell>
                  <TableCell align="center" style={{ fontWeight: "bold" }}>
                    Last Name
                    <span
                      onClick={() => {
                        setSortby({
                          ...sortBy,
                          last_name: !sortBy?.last_name,
                        });
                        handleSort("last_name");
                      }}
                    >
                      {sortBy?.last_name ? (
                        <ArrowUpwardOutlinedIcon fontSize="16" />
                      ) : (
                        <SouthOutlinedIcon fontSize="16" />
                      )}
                    </span>
                  </TableCell>
                  <TableCell align="center" style={{ fontWeight: "bold" }}>
                    Email
                    <span
                      onClick={() => {
                        setSortby({
                          ...sortBy,
                          email: !sortBy?.email,
                        });
                        handleSort("email");
                      }}
                    >
                      {sortBy?.email ? (
                        <ArrowUpwardOutlinedIcon fontSize="16" />
                      ) : (
                        <SouthOutlinedIcon style={{ fontSize: "16px" }} />
                      )}
                    </span>
                  </TableCell>
                  <TableCell align="center" style={{ fontWeight: "bold" }}>
                    Mobile Number
                    <span
                      onClick={() => {
                        setSortby({
                          ...sortBy,
                          mobile_no: !sortBy?.mobile_no,
                        });
                        handleSort("mobile_no");
                      }}
                    >
                      {sortBy?.mobile_no ? (
                        <ArrowUpwardOutlinedIcon fontSize="16" />
                      ) : (
                        <SouthOutlinedIcon fontSize="16" />
                      )}
                    </span>
                  </TableCell>
                  <TableCell align="center" style={{ fontWeight: "bold" }}>
                    Gender
                    <span
                      onClick={() => {
                        setSortby({
                          ...sortBy,
                          gender: !sortBy?.gender,
                        });
                        handleSort("gender");
                      }}
                    >
                      {sortBy?.gender ? (
                        <ArrowUpwardOutlinedIcon fontSize="16" />
                      ) : (
                        <SouthOutlinedIcon fontSize="16" />
                      )}
                    </span>
                  </TableCell>
                  <TableCell align="center" style={{ fontWeight: "bold" }}>
                    Action
                  </TableCell>
                </TableRow>
              </TableHead>
              <TableBody>
                {globalStateData
                  ?.filter((val) => {
                    if (searchedData == "") {
                      return val;
                    } else if (
                      val?.first_name
                        .toLowerCase()
                        .includes(searchedData?.toLowerCase())
                    ) {
                      return val;
                    }
                  })
                  ?.map((row) => (
                    <TableRow
                      key={row?.name}
                      sx={{ "&:last-child td, &:last-child th": { border: 0 } }}
                    >
                      <TableCell align="center">{row?.first_name}</TableCell>
                      <TableCell align="center">{row?.last_name}</TableCell>
                      <TableCell align="center">{row?.email}</TableCell>
                      <TableCell align="center">{row?.mobile_no}</TableCell>
                      <TableCell align="center">Male</TableCell>
                      <TableCell align="center">
                        <EditIcon
                          style={{ cursor: "pointer" }}
                          onClick={() => onEditHandler(row?.id, row)}
                        />
                        <DeleteIcon
                          style={{ cursor: "pointer" }}
                          onClick={() => {
                            onDeleteUser(row?.id);
                          }}
                          sx={{ color: red[700] }}
                        />
                        <RemoveRedEyeOutlinedIcon
                          onClick={() => onViewHandler(row?.id)}
                        />
                      </TableCell>
                    </TableRow>
                  ))}
              </TableBody>
            </Table>
          </TableContainer>
        </Grid>
        <TablePagination
          rowsPerPageOptions={[10, 25, 100]}
          component="div"
          count={rows.length}
          rowsPerPage={rowsPerPage}
          page={page}
          onPageChange={handleChangePage}
          onRowsPerPageChange={handleChangeRowsPerPage}
        />
      </Container>
      {deleteModal && (
        <DeleteModal
          setDelteModal={(val) => setDelteModal(val)}
          deleteModall={deleteModal}
          deleteId={deleteId}
        />
      )}
    </>
  );
};

// function mapStateToProps({ getUsers }) {
//   const { loadingUserList, users } = getUsers;
//   return { loadingUserList, users };
// }

// export default connect(mapStateToProps, { getUserList })(Landing);
export default Landing;
