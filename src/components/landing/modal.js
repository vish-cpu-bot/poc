import Dialog from "@mui/material/Dialog";
import DialogActions from "@mui/material/DialogActions";
import { red } from "@mui/material/colors";
import DeleteIcon from "@mui/icons-material/Delete";
import DialogTitle from "@mui/material/DialogTitle";
import { Button } from "@mui/material";
import DialogContent from "@mui/material/DialogContent";
import DialogContentText from "@mui/material/DialogContentText";
import { useDispatch } from "react-redux";
import { deleteUser } from "../../redux/actions";

const DeleteModal = (props) => {
  const { deleteModall, setDelteModal, deleteId } = props;
  const dispatch = useDispatch();
  const handleDelete = () => {
    setDelteModal(false);
    dispatch(deleteUser(deleteId));
  };
  return (
    <div>
      <Dialog
        open={deleteModall}
        aria-labelledby="alert-dialog-title"
        aria-describedby="alert-dialog-description"
      >
        <DialogTitle id="alert-dialog-title">
          {"Are you sure you want to delete the user"}
        </DialogTitle>
        <DialogContent>
          <DialogContentText id="alert-dialog-description">
            This User will be permanently deleted. Do you want to proceed?
          </DialogContentText>
        </DialogContent>
        <DialogActions>
          <Button onClick={() => setDelteModal(false)}>Cancel</Button>
          <DeleteIcon onClick={() => handleDelete()} sx={{ color: red[700] }} />
        </DialogActions>
      </Dialog>
    </div>
  );
};

export default DeleteModal;
