// Base Url

export const BASE_URL = "http://localhost:8000/api/v1";

//User End Points
export const API = {
  userList: `${BASE_URL}/users`,
  deleteUser: `${BASE_URL}/users`,
  addUser: `${BASE_URL}/users`,
  getUser: `${BASE_URL}/users`,
  editUser: `${BASE_URL}/users`,
};
